# Interest Camera Demo

This project is a demo to show the InterestCamera script. It makes the object
it's attached to follow a particular object or position of interest.
